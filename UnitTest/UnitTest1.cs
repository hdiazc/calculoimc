﻿using System;
using Imc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class IndiceMasaCorporalTests
    {
        [TestMethod]
        public void TestCalcularImc()
        {
            //Arrange
            var indiceMasaCorporal = new IndiceMasaCorporal();
            double expectedResult = 20;
        
            //Act
            double actualResult = indiceMasaCorporal.CalcularImc(30, 1.3);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_CasoDesnutricion()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene desnutrición";

            //Act
            string actualResult = indiceMasa.IndiceMasa(30, 1.3);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_DivisionPorCero()
        {
            //Arrange
            var indiceMalo = new IndiceMasaCorporal();
            string expectedResult = "Valores no válidos";

            //Act
            string actualResult = indiceMalo.IndiceMasa(10, 0);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_ValorNoValidoNegativo()
        {
            //Arrange
            var indiceMalo = new IndiceMasaCorporal();
            string expectedResult = "Valores no válidos";

            //Act
            string actualResult = indiceMalo.IndiceMasa(-10, -56);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void TestIndiceMasa_PesoNormal()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene peso normal";

            //Act
            string actualResult = indiceMasa.IndiceMasa(65, 1.65);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_Sobrepeso()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene sobrepeso";

            //Act
            string actualResult = indiceMasa.IndiceMasa(80, 1.7);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_Obesidad1()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene obesidad grado 1";

            //Act
            string actualResult = indiceMasa.IndiceMasa(100, 1.8);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_Obesidad2()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene obesidad grado 2";

            //Act
            string actualResult = indiceMasa.IndiceMasa(120, 1.85);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestIndiceMasa_ObesidadMorbida()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene obesidad mórbida";

            //Act
            string actualResult = indiceMasa.IndiceMasa(120, 1.62);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
        [TestMethod]
        public void TestIndiceMasa_ObesidadMorbida2()
        {
            //Arrange
            var indiceMasa = new IndiceMasaCorporal();
            string expectedResult = "Tiene peso normal";

            //Act
            string actualResult = indiceMasa.IndiceMasa(50, 1.62);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
