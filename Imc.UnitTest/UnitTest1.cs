﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Imc;

namespace UnitTest
{
    [TestClass]
    public class CalculadoraTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            var calculadora = new Calculadora();
            float expectedResult = 17.8f;

            //Act
            float actualResult = calculadora.CalcularImc(30, 1.3);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
