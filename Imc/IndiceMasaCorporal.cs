﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imc
{
    public class IndiceMasaCorporal
    {
        private double masa;
        private double estatura;
        private double indice;

        public IndiceMasaCorporal()
        {

        }

        public double CalcularImc(double masa, double estatura)
        {
            //revisa que las condiciones sean válidas
            if (masa <= 0 || estatura <= 0)
            {
                return -1;
            }

            estatura = Math.Pow(estatura, 2);
            indice = Math.Round((masa / estatura), 2);
            return indice;
            
        }

        public string IndiceMasa(double masa, double estatura)
        {
            indice = CalcularImc(masa, estatura);

            if (indice == -1)
            {
                return "Valores no válidos";
            }

            if (indice < 18)
            {
                return "Tiene desnutrición";
            } else if (indice < 25 )
            {
                return "Tiene peso normal";
            } else if (indice < 30)
            {
                return "Tiene sobrepeso";
            } else if (indice < 35)
            {
                return "Tiene obesidad grado 1";
            } else if (indice < 40)
            {
                return "Tiene obesidad grado 2";
            }
            else
            {
                return "Tiene obesidad mórbida";
            }
        }
    }
}
